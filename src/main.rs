#[macro_use]
extern crate serde;
use avro_rs::from_avro_datum;
use futures::TryStreamExt;
use pulsar::reader;
use pulsar::{
    consumer::ConsumerOptions, message::proto, reader::Reader,consumer::Consumer, Authentication, DeserializeMessage,
    Payload, Pulsar, TokioExecutor,
};
use std::env;
use std::io::Cursor;

mod avro_schema;
use crate::avro_schema::{DEFI_ALERT_DEFINITION, INSTRUMENT_LIST_DEFINITION, TICKER_PRICE_DEF, TEST_DEFI_ALERT_DEFINITION};

use avro_rs::Schema;
use avro_rs::from_value;
use avro_rs::Reader as avro_reader;


#[derive(Serialize, Deserialize)]
struct TestData {
    data: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct MyTestData {
    block: i64,
    blockchain: String,
    counterparty: String,
    from_addr: String,
    message: String, 
    tag: String,
    timestamp: i64,
    txid: String,
    r#type: String
}


impl DeserializeMessage for MyTestData {
    type Output = anyhow::Result<()>;

    fn deserialize_message(payload: &Payload) -> Self::Output {
        println!("{:?}", payload);
        // let mut payload = payload.clone();
        let writer_schema = Schema::parse_str(DEFI_ALERT_DEFINITION).unwrap();
        let reader_schema = Schema::parse_str(DEFI_ALERT_DEFINITION).unwrap();

        // let reader = avro_reader::new(&payload.data[..]).unwrap();

        // reader creation can fail in case the input to read from is not Avro-compatible or malformed
        // let reader = avro_reader::with_schema(&reader_schema, &payload.data[..]).unwrap();
        // for value in reader {
        //     println!("{:?}", value.unwrap());
        // }
        let output = from_avro_datum(&writer_schema, & mut Cursor::new(payload.data.clone()), Some(&reader_schema));
        println!("{:?}", output);
        println!("Hi");

        Ok(())
        }
}
#[tokio::main]
async fn main() -> Result<(), pulsar::Error> {
    env_logger::init();

    let addr = env::var("PULSAR_HOST")
        .ok()
        .unwrap_or_else(|| "pulsar://127.0.0.1:6650".to_string());
    let topic = env::var("PULSAR_TOPIC")
        .ok()
        .unwrap_or_else(|| "non-persistent://public/default/test".to_string());

    let mut builder = Pulsar::builder(addr, TokioExecutor);

    if let Ok(token) = env::var("PULSAR_JWT") {
        let authentication = Authentication {
            name: "token".to_string(),
            data: token.into_bytes(),
        };

        builder = builder.with_auth(authentication);
    }

    // Just in case
    let _schema = Schema::parse_str(DEFI_ALERT_DEFINITION).unwrap();
    println!("{:?}", _schema);



    let pulsar: Pulsar<_> = builder.build().await?;

    // let consumer_options = ConsumerOptions {
    //     schema: Some(proto::Schema {
    //         r#type: proto::schema::Type::Avro as i32,
    //         schema_data: INSTRUMENT_LIST_DEFINITION.as_bytes().into(),
    //         ..Default::default()
    //     }),
    //     ..Default::default()
    // };

    let mut consumer: Consumer<MyTestData, _> = pulsar
    .consumer()
    .with_topic(topic)
    .with_consumer_name("test_consumer_3")
    .with_subscription_type(proto::command_subscribe::SubType::Exclusive)
    .with_subscription("test_subscription_3")
    .build()
    .await?;

let mut counter = 0usize;
println!("Got here");

while let Some(msg) = consumer.try_next().await? {
    consumer.ack(&msg).await?;
    println!("Got here 2.");

    let _data = match msg.deserialize() {
        Ok(data) => data,
        Err(e) => {
            log::error!("could not deserialize message: {:?}", e);
            break;
        }
    };

    counter += 1;
    log::info!("got {} messages", counter);

    if counter > 2 {
        break;
    }
}


    Ok(())
}