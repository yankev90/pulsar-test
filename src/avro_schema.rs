pub static DEFI_ALERT_DEFINITION: &str = r#"
{
    "type": "record",
    "name": "DefiAlertMessage",
    "fields": [
        {"name": "block",       "type": "long"},
        {"name": "blockchain",  "type": "string"},
        {"name": "counterparty","type": "string"},
        {"name": "from_addr",   "type": "string"},
        {"name": "message",     "type": "string"},
        {"name": "tag",         "type": "string"},
        {"name": "timestamp",   "type": "long"},
        {"name": "txid",        "type": "string"},
        {"name": "type",        "type": "string"}
    ]
}
"#;

pub static TEST_DEFI_ALERT_DEFINITION: &str = r#"
{
    "type": "record",
    "name": "DefiAlertMessage",
    "fields": [
        {"name": "blockchain",  "type": "string"},
        {"name": "counterparty","type": "string"}
    ]
}
"#;

pub static INSTRUMENT_LIST_DEFINITION: &str = r#"
{
    "type": "record",
    "name": "instrument_list",
    "fields": [
        {"name": "instruments", "type": "string"},
        {"name": "stream", "type": "string"},
        {"name": "timestamp_ms", "type": "long"}
    ]
}
"#;

pub static TICKER_PRICE_DEF: &str = r#"
{
    "type": "record",
    "name": "price_update",
    "fields": [
        {"name": "timestamp", "type": "int"},
        {"name": "ticker", "type": "string"},
        {"name": "exchange", "type": "string"},
        {"name": "best_bid_price", "type": "float"},
        {"name": "best_ask_price", "type": "float"},
        {"name": "best_bid_amount", "type": "float"},
        {"name": "best_ask_amount", "type": "float"},
        {"name": "last_price", "type": "float"},
        {"name": "msg_id_tuple_str", "type": "string"}
    ]
}
"#;